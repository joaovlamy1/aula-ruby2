#Declarando classe Aluno
class Aluno
    #Todos os atributos
      attr_accessor :matricula, :periodoletivo, :curso, :turmas
  
      #Declarando valores dos atributos
      def initialize(matricula, periodoletivo, curso)
          @matricula = matricula
          @periodoletivo = periodoletivo
          @curso = curso
          @turmas = []
      end
      
      #Declarando metodo de aluno
      def inscrever(nome_turma)
          #Ao increver-se a turma é adcionado ao array turmas do aluno
          @turmas.push(nome_turma)
      end
  
  end
  
  #Declarando classe Usuarios
  class Usuarios
    #Todos os atributos
      attr_accessor :email, :senha, :nome, :nascimento, :logado
      #Declarando valores dos atributos
      def initialize(email, senha, nome, nascimento)
          @email = email
          @senha = senha
          @nome = nome
          @nascimento = nascimento 
          @logado = true
      end
      #Declarando metodos de Usuarios
      def idade()
        #A idade é descoberta subtraindo a data de hoje pelo nascimento
          @nascimento = Date.now - (nascimento[6,4].to_i)
          return @nascimento
      end
  
      def logar(senha_digitada)
        #O usuario pode logar se a senha estiver certa
          if senha_digitada = @senha
              @logado = true
          end
      end
  
      def deslogar()
        #Se o aluno deslogar o atributo logado recebe false
          @logado = false
      end
  
  end
  
  #Declarando classe Turma
  class Turma
    #Todos os atributos
      attr_accessor :nome, :horario, :dias_da_semana, :inscritos, :inscrição_aberta
      #Declarando valores dos atributos
      def initialize(nome, horario, dias_da_semana, inscritos, inscrição_aberta)
          @nome = nome
          @horario = horario
          @dias_da_semana = []
          @inscritos = []
          @inscrição_aberta = false
      end
      #Declarando metodos de Turma
      def abrir_inscrição()
        #quando a inscrição é aberta o atributo inscrição_aberta recebe true
          @inscrição_aberta = true
      end
  
      def fechar_inscrição()
        #quando a inscrição é fechada o atributo inscrição_aberta recebe false
          @inscrição_aberta = false
      end
  
      def adicionar_aluno(nome_aluno)
        #quando um aluno se inscreve na aula o nome dele é adcionado ao atributo inscritos
          @inscritos.push(nome_aluno)
      end
  
  end
  
  #Declarando classe Materia
  class Materia
    #Todos os atributos
      attr_accessor :ementa, :nome, :professores
      #Declarando valores dos atributos
      def initialize(ementa, nome)
          @ementa = ementa
          @nome = nome
          @professores = []
      end
      #Declarando metodo de Materia
      def adicionar_professor(nome_professor)
        #quando um Professor adcionado a materia o nome dele é adcionado ao atributo professores
          @professores.push(nome_professor)
      end
  end
  
  #Declarando classe Professor
  class Professor
    #Todos os atributos
      attr_accessor :matricula, :salario, :materias
      #Declarando valores dos atributos
      def initialize(matricula, salario)
          @matricula = matricula
          @salario = salario.to_f
          @materias = []
      end
      #Declarando metodo de Materia
      def adicionar_materia(nome_materia)
        #quando um Professor adcionado a materia o nome dela é adcionado ao atributo materias
          @materias.push(nome_materia)
      end
  end
  # S2 Tudo Declarado!!! S2